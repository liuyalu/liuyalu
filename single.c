/*************************************************************************
    > File Name: single.c
    > Author: ma6174
    > Mail: ma6174@163.com 
    > Created Time: 2014年07月09日 星期三 18时07分43秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
void bug(float x)\
{
	printf("bug %f\n",x);
}
// declare 定义
typedef struct Node{
	int val;
	struct Node *next;
}Node;
typedef Node *List;
List L;
// insert list
// 在单链表L的第i个节点之前插入e
int  InsertList(int i,int e)
{
	List p,s;int j;
	p=L;j=0;
//	bug(1);
	while(p && j<i-1)
	{
		p = p->next;
		++j;
	}
	if(!p || j > i-1)
	{
		return 0;
	}
//	bug(2);
	s = (List)malloc(sizeof(Node));
//	bug(2.1);
	if(s == NULL) exit(0);
	s->val = e;
//	bug(2.2);
	s->next = p->next;
//	bug(2.3);
	p->next = s;
//	bug(3);
	return 1;
}
//delete node 删除单链表L的第i个节点并用e返回其值
int  DeleteList(int i,int e)
{
	List p,s;int j;
	p = L;j = 0;
	while(p && j < i-1)
	{
		p = p->next;
		++j;
	}
	if(!p || j > i-1) return 0;
	s = p->next;
	p->next = s->next;
	e = s->val;
	free(s);
	return 1;
}
// find the length of the list  表长多少
int ListLength()
{
	int length = 0;
	List p = L->next;
	while(p)
	{
		length++;
		p = p->next;
	}
	return length;
}
//看单链表中有无x值  if x  exist in the list,return the position
int  LocateElem(int e)
{
	int cnt = 0;
	List p = L->next;
	while(p)
	{
		cnt++;
		if(p->val == e) return cnt;
		p = p->next;
	}
	return 0;
}
//打印链表  print the list
void PrintList()
{
	printf("The list is:  \n");
	List p = L->next;
	//bug(1);
	while(p)
	{
		printf("%d  ",p->val);
		p = p->next;
	}
	//bug(2);
	printf("\n");
}
// destroy the L
void DestroyList()
{
	List p;
	while(L)
	{
		p = L->next;
		free(L);
		L = p;
	}
}
//clear the list
void ClearList()
{
	List p = L->next;
	L->next = NULL;
	DestroyList(p);
}
int main()
{
	Node temp_node;
	int opt;int length,data;
	printf("what do you want to do?please input the opt\n");
	printf("1 建立链表 2 插入链表 3 删除节点 4 查找 5打印  6退出\n");
	int exist = 0;
	while(scanf("%d",&opt))
	{
		if(opt == 1)
		{
	//		CreateList(&L);
	        if(exist) 
			{
                 printf("exist!\n");
				 continue;
			}
			exist = 1;
	        L = (List)malloc(sizeof(Node));
	        if(L == NULL)
	        {
		        exit(0);
	        }
	        L->next = NULL;
			printf("input the length\n");
			scanf("%d",&length);
			int i;
			for(i=1;i<=length;++i)
			{
				printf("输入第%d个元素\n",i);
				scanf("%d",&data);
			//	bug(i);
				InsertList(i,data);
			}
			printf("success!\n");
			PrintList();
		}
		else if(opt == 2)
		{
			printf("input the position and the data!\n");
			int pos;
			scanf("%d %d",&pos,&data);
			InsertList(pos,data);
			PrintList();
		}
		else if(opt == 3)
		{
			printf("input the position!\n");
			int pos;
			scanf("%d",&pos);
			int right = DeleteList(pos,data);
			if(right == 0) 
			{
				printf("error!\n");
				continue;
			}
			printf("the data which you delete is %d\n",data);
			PrintList();
		}
		else if(opt == 4)
		{
			printf("input the data which you want to find\n");
			scanf("%d",&data);
			int pos = LocateElem(data);
			if(pos)
			{
				printf("the position is  %d\n",pos);
			}
			else
			{
				printf("not found!");
			}
			PrintList();
		}
		else if(opt == 5)
		{
			PrintList();
		}
		else if(opt == 6)
		{
			return 0;
		}
	printf("1 建立链表 2 插入链表 3 删除节点 4 查找 5打印  6退出\n");
	}
	return 1;
}

