/*************************************************************************
    > File Name: single.c
    > Author: liuyalu
    > Mail: 13844098386@163.com 
    > Created Time: 2014年07月09日 星期三 18时07分43秒
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

void bug(float x)
{
	printf("bug %f\n",x);
}

// declare 定义
typedef struct Node{
	int val;
	struct Node *next;
}Node;
typedef Node *List;
List L;

// insert list
int  InsertList(int i,int e)
{
	List p,s;int j;
	p=L;j=0;
	while(p && j<i-1)
	{
		p = p->next;
		++j;
	}
	if(!p || j > i-1)
	{
		return 0;
	}
	s = (List)malloc(sizeof(Node));
	if(s == NULL) exit(0);
	s->val = e;
	s->next = p->next;
	p->next = s;
	return 1;
}

//delete node ,return the value with e
int  DeleteList(int i,int e)
{
	List p,s;int j;
	p = L;j = 0;
	while(p && j < i-1)
	{
		p = p->next;
		++j;
	}
	if(!p || j > i-1) return 0;
	s = p->next;
	p->next = s->next;
	e = s->val;
	free(s);
	return 1;
}

// find the length of the list  
int ListLength()
{
	int length = 0;
	List p = L->next;
	while(p)
	{
		length++;
		p = p->next;
	}
	return length;
}

//if x  exist in the list,return the position
int  LocateElem(int e)
{
	int cnt = 0;
	List p = L->next;
	while(p)
	{
		cnt++;
		if(p->val == e) return cnt;
		p = p->next;
	}
	return 0;
}

// print the list
void PrintList()
{
	printf("The list is:  \n");
	List p = L->next;
	//bug(1);
	while(p)
	{
		printf("%d  ",p->val);
		p = p->next;
	}
	//bug(2);
	printf("\n");
}

// destroy the L
void DestroyList()
{
	List p;
	while(L)
	{
		p = L->next;
		free(L);
		L = p;
	}
}

//clear the list
void ClearList()
{
	List p = L->next;
	L->next = NULL;
	DestroyList(p);
}

int main(void)
{
	Node temp_node;
	int opt;int length,data;
	FILE *fp;
	fp = fopen("data.in","r");
	printf("what do you want to do?please input the opt\n");
	printf("1 建立链表 2 插入链表 3 删除节点 4 查找 5打印  6退出\n");
	int exist = 0;
	while(fscanf(fp,"%d",&opt))
	{
		if(opt == 1)
		{
	//		CreateList(&L);
	        if(exist) 
			{
                 printf("exist!\n");
				 continue;
			}
			exist = 1;
	        L = (List)malloc(sizeof(Node));
	        if(L == NULL)
	        {
		        exit(0);
	        }
	        L->next = NULL;
			printf("input the length\n");
			fscanf(fp,"%d",&length);
			int i;
			for(i=1;i<=length;++i)
			{
				printf("输入第%d个元素\n",i);
				fscanf(fp,"%d",&data);
			//	bug(i);
				InsertList(i,data);
			}
			printf("success!\n");
			PrintList();
		}
		else if(opt == 2)
		{
			printf("input the position and the data!\n");
			int pos;
			fscanf(fp,"%d %d",&pos,&data);
			int right = InsertList(pos,data);
			if(right == 0) 
			{
				printf("操作失败！\n");
				continue;
			}
			PrintList();
		}
		else if(opt == 3)
		{
			printf("input the position!\n");
			int pos;
			fscanf(fp,"%d",&pos);
			int right = DeleteList(pos,data);
			if(right == 0) 
			{
				printf("error!\n");
				continue;
			}
			printf("the data which you delete is %d\n",data);
			PrintList();
		}
		else if(opt == 4)
		{
			printf("input the data which you want to find\n");
			fscanf(fp,"%d",&data);
			int pos = LocateElem(data);
			if(pos)
			{
				printf("the position is  %d\n",pos);
			}
			else
			{
				printf("not found!");
			}
			PrintList();
		}
		else if(opt == 5)
		{
			PrintList();
		}
		else if(opt == 6)
		{
			fclose(fp);
			return 0;
		}

	printf("1 建立链表 2 插入链表 3 删除节点 4 查找 5打印  6退出\n");
	}
	return 1;
}

